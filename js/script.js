/*1. setTimeout() - запускає функцію через зазначений час; setInterval() - запускає функцію циклічно через зазначений час.
  2. Планується до запуску відразу після поточного скрипту. Нульова затримка насправді не дорівнює нулю.
  3. Аби припинити подальші виклики. SetTimeout додає виклик у чергу, якщо навіть перепиcати timer, то попередньо запланований виклик все одно буде виконано при добіганні інтервалу.
*/
let stopbtn = document.querySelector('.stop'), startBtn = document.querySelector('.start'), counter = 1
startBtn.setAttribute('disabled', 'disabled')
let banners = () => {
  let ChangeImage = setInterval(() => {
    if (counter > 4) { counter = 1 }
    document.querySelector('.image-to-show').src = `./img/${counter}.jpg`
    counter++
  }, 3000);

stopbtn.addEventListener('click', () => {
  clearInterval(ChangeImage)
  startBtn.removeAttribute('disabled')
  })}
banners()
startBtn.addEventListener('click', () => {
  banners()
startBtn.setAttribute('disabled', 'disabled')
})